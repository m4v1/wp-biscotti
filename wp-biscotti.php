<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.olos.com
 * @since             1.0.0
 * @package           Wp_Biscotti
 *
 * @wordpress-plugin
 * Plugin Name:       Biscotti GDPR
 * Plugin URI:        https://www.olos.com
 * Description:       Plugin per la gestione dei banner GDPR.
 * Version:           1.2.5
 * Author:            Marco Vivi
 * Author URI:        https://www.olos.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-biscotti
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (! defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('PLUGIN_NAME_VERSION', '1.2.5');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-biscotti-activator.php
 */
function activate_wp_biscotti()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-wp-biscotti-activator.php';
    Wp_Biscotti_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-biscotti-deactivator.php
 */
function deactivate_wp_biscotti()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-wp-biscotti-deactivator.php';
    Wp_Biscotti_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_wp_biscotti');
register_deactivation_hook(__FILE__, 'deactivate_wp_biscotti');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-wp-biscotti.php';

/**
 * A custom update checker for WordPress plugins.
 *
 * Useful if you don't want to host your project
 * in the official WP repository, but would still like it to support automatic updates.
 * Despite the name, it also works with themes.
 *
 * @link http://w-shadow.com/blog/2011/06/02/automatic-updates-for-commercial-themes/
 * @link https://github.com/YahnisElsts/plugin-update-checker
 * @link https://github.com/YahnisElsts/wp-update-server
 */
if (is_admin()) {
    if (! class_exists('Puc_v4_Factory')) {
        require_once 'plugin-update-checker/plugin-update-checker.php';
    }
    $WpbUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://gitlab.com/m4v1/wp-biscotti/', //Repo URL.
        __FILE__, //Full path to the main plugin file.
        'wp-biscotti' //Plugin slug.
    );
    $WpbUpdateChecker->setBranch('master');
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_biscotti()
{

    $plugin = new Wp_Biscotti();
    $plugin->run();
}
run_wp_biscotti();
