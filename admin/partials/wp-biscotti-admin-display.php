<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.olos.com
 * @since      1.0.0
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    
    <h1 class="wp-heading-inline">Wordpress GDPR Banner</h1>
    
    <form method="post" name="gdpr_options" action="options.php">
        
        <?php settings_fields($this->plugin_name); ?>
        
        <?php
            //Grab all options
            $options = get_option($this->plugin_name);

            // Modal customization vars
            $modal_background_color = $options['modal_background_color'];
            $modal_text_color = $options['modal_text_color'];
            $modal_primary_color = $options['modal_primary_color'];
            
            // Text contents
            $banner_content_standard = $options['banner_content_standard'];
            $banner_content_why = $options['banner_content_why'];
            $banner_content_confirm = $options['banner_content_confirm'];
            $banner_content_limited = $options['banner_content_limited'];
            $head_code_using_cookies = $options['head_code_using_cookies'];
            $body_code_using_cookies = $options['body_code_using_cookies'];
            
            // Font Sizes
            $wpb_title_size = $options['modal_title-size'];
            $wpb_content_size = $options['modal_content-size'];
            $wpb_button_size = $options['modal_button-size'];
            $wpb_subbtn_size = $options['modal_subbtn-size'];
            
            // Modal is active
            $wpb_modal_active = $options['modal_active'];
        ?>
        
        <h2 class="nav-tab-wrapper">
            <button id="wpb-tab-1-btn" class="nav-tab nav-tab-active">Aspetto</button>
            <button id="wpb-tab-2-btn" class="nav-tab">Testi dei banner</button>
            <button id="wpb-tab-3-btn" class="nav-tab">Codice di Analytics</button>
        </h2>
        
        <div id="wpb-page-1" class="wpb-settings-page wpb-page-open">

                <h2 class="wp-heading-inline"><?php _e('Colore di background del banner', $this->plugin_name);?></h2>
                <fieldset class="wp_biscotti-admin-colors">
                    <input type="text" class="<?php echo $this->plugin_name;?>-color-picker" id="<?php echo $this->plugin_name;?>-modal_background_color" name="<?php echo $this->plugin_name;?>[modal_background_color]" value="<?php echo $modal_background_color;?>" />
                </fieldset>
                
                <h2 class="wp-heading-inline"><?php _e('Colore del testo del banner', $this->plugin_name);?></h2>
                <fieldset class="wp_biscotti-admin-colors">
                    <input type="text" class="<?php echo $this->plugin_name;?>-color-picker" id="<?php echo $this->plugin_name;?>-modal_text_color" name="<?php echo $this->plugin_name;?>[modal_text_color]" value="<?php echo $modal_text_color;?>" />
                </fieldset>
                
                <h2 class="wp-heading-inline"><?php _e('Colore del bottone del banner', $this->plugin_name);?></h2>
                <fieldset class="wp_biscotti-admin-colors">
                    <input type="text" class="<?php echo $this->plugin_name;?>-color-picker" id="<?php echo $this->plugin_name;?>-modal_primary_color" name="<?php echo $this->plugin_name;?>[modal_primary_color]" value="<?php echo $modal_primary_color;?>" />
                </fieldset>

                <h2 class="wp-heading-inline"><?php _e('Dimensione dei font', $this->plugin_name);?></h2>
                
                <fieldset class="wp_biscotti-admin-fonts-size">
                    <input type="text" class="<?php echo $this->plugin_name;?>-title-size" id="<?php echo $this->plugin_name;?>-modal_title-size" name="<?php echo $this->plugin_name;?>[modal_title-size]" value="<?php echo $wpb_title_size;?>" />
                    <label for="<?php echo $this->plugin_name;?>-modal_title-size">Dimensione font del titolo (in px o rem)</label>
                </fieldset>
                
                <fieldset class="wp_biscotti-admin-fonts-size">
                    <input type="text" class="<?php echo $this->plugin_name;?>-content-size" id="<?php echo $this->plugin_name;?>-modal_content-size" name="<?php echo $this->plugin_name;?>[modal_content-size]" value="<?php echo $wpb_content_size;?>" />
                    <label for="<?php echo $this->plugin_name;?>-modal_content-size">Dimensione font del contenuto (in px o rem)</label>
                </fieldset>
                
                <fieldset class="wp_biscotti-admin-fonts-size">
                    <input type="text" class="<?php echo $this->plugin_name;?>-button-size" id="<?php echo $this->plugin_name;?>-modal_button-size" name="<?php echo $this->plugin_name;?>[modal_button-size]" value="<?php echo $wpb_button_size;?>" />
                    <label for="<?php echo $this->plugin_name;?>-modal_button-size">Dimensione font del bottone (in px o rem)</label>
                </fieldset>
                
                <fieldset class="wp_biscotti-admin-fonts-size">
                    <input type="text" class="<?php echo $this->plugin_name;?>-content-subbtn-size" id="<?php echo $this->plugin_name;?>-modal_subbtn-size" name="<?php echo $this->plugin_name;?>[modal_subbtn-size]" value="<?php echo $wpb_subbtn_size;?>" />
                    <label for="<?php echo $this->plugin_name;?>-modal_subbtn-size">Dimensione font del link in basso (in px o rem)</label>
                </fieldset>
            
        </div>
        
        <div id="wpb-page-2" class="wpb-settings-page wpb-page-hidden">
            <h2 class="wp-heading-inline"><?php _e('Testo del banner (Principale)', $this->plugin_name);?></h2>
            <?php
            $settings = array('textarea_name' => $this->plugin_name . '[banner_content_standard]', 'textarea_rows' => 15);
            wp_editor($banner_content_standard, 'banner_content_standard', $settings);
            ?>
            
            <h2 class="wp-heading-inline"><?php _e('Testo del banner (Perché)', $this->plugin_name);?></h2>
            <?php
            $settings = array('textarea_name' => $this->plugin_name . '[banner_content_why]', 'textarea_rows' => 15);
            wp_editor($banner_content_why, 'banner_content_why', $settings);
            ?>
            
            <h2 class="wp-heading-inline"><?php _e('Testo del banner (Conferma)', $this->plugin_name);?></h2>
            <?php
            $settings = array('textarea_name' => $this->plugin_name . '[banner_content_confirm]', 'textarea_rows' => 15);
            wp_editor($banner_content_confirm, 'banner_content_confirm', $settings);
            ?>
            
            <h2 class="wp-heading-inline"><?php _e('Testo del banner (Navigazione limitata)', $this->plugin_name);?></h2>
            <?php
            $settings = array('textarea_name' => $this->plugin_name . '[banner_content_limited]', 'textarea_rows' => 15);
            wp_editor($banner_content_limited, 'banner_content_limited', $settings);
            ?>
        </div>
        
        <div id="wpb-page-3" class="wpb-settings-page wpb-page-hidden">
            <h2 class="wp-heading-inline"><?php _e('Codice da inserire nel tag HEAD', $this->plugin_name);?></h2>
            <?php
            $settings = array('tinymce' => false, 'media_buttons' => false, 'textarea_name' => $this->plugin_name . '[head_code_using_cookies]');
            wp_editor($head_code_using_cookies, 'head_code_using_cookies', $settings);
            ?>
            
            <h2 class="wp-heading-inline"><?php _e('Codice da inserire nel tag BODY', $this->plugin_name);?></h2>
            <?php
            $settings = array('tinymce' => false, 'media_buttons' => false, 'textarea_name' => $this->plugin_name . '[body_code_using_cookies]');
            wp_editor($body_code_using_cookies, 'body_code_using_cookies', $settings);
            ?>
        </div>
        
        <h2 class="wp-heading-inline"><?php _e('Attivazione del banner', $this->plugin_name);?></h2>
        <fieldset>
            <legend class="screen-reader-text"><span>Attivare il plugin</span></legend>
            <label for="users_can_register">
                <input type="checkbox" name="<?php echo $this->plugin_name;?>[modal_active]" value="1" <?php echo $wpb_modal_active ? 'checked' : '' ?>/>
                <span><?php esc_attr_e('Banner attivo', $this->plugin_name); ?></span>
            </label>
        </fieldset>

        <?php submit_button(__('Save all changes', $this->plugin_name), 'primary', 'submit', true); ?>

        </form>
    
        </div>