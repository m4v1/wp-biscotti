(function( $ ) {
	'use strict';

	$(function(){

         var colorPickerInputs = $( '.wp-biscotti-color-picker' );
         // WordPress specific plugins - color picker
         $( '.wp-biscotti-color-picker' ).wpColorPicker();
		 
		 $( "button#wpb-tab-1-btn" ).not("nav-tab-active").on( "click", function(e) {
		  	e.preventDefault();
		    $(".wpb-page-open").toggleClass("wpb-page-open wpb-page-hidden");
 	        $( "#wpb-page-1" ).toggleClass("wpb-page-open wpb-page-hidden");
		    $( ".nav-tab-active" ).removeClass("nav-tab-active");
		    $( "#wpb-tab-1-btn" ).addClass("nav-tab-active");
		    this.blur();
 	     });
		 
		 $( "button#wpb-tab-2-btn" ).not("nav-tab-active").on( "click", function(e) {
			 e.preventDefault();
			 $(".wpb-page-open").toggleClass("wpb-page-open wpb-page-hidden");
 	       	 $( "#wpb-page-2" ).toggleClass("wpb-page-open wpb-page-hidden");
		   	 $( ".nav-tab-active" ).removeClass("nav-tab-active");
		   	 $( "#wpb-tab-2-btn" ).addClass("nav-tab-active");
		   	 this.blur();
 	     });
		 
		 $( "button#wpb-tab-3-btn" ).not("nav-tab-active").on( "click", function(e) {
			 e.preventDefault();
			 $(".wpb-page-open").toggleClass("wpb-page-open wpb-page-hidden");
 	         $( "#wpb-page-3" ).toggleClass("wpb-page-open wpb-page-hidden");
		     $( ".nav-tab-active" ).removeClass("nav-tab-active");
		     $( "#wpb-tab-3-btn" ).addClass("nav-tab-active");
		     this.blur();
 	     });

    }); // End of DOM Ready

})( jQuery );
