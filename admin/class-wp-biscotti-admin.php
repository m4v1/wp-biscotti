<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.olos.com
 * @since      1.0.0
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/admin
 * @author     Marco Vivi <marco.vivi@gmail.com>
 */
class Wp_Biscotti_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wp_Biscotti_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wp_Biscotti_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if ('settings_page_wp-biscotti' == get_current_screen() -> id) {
             // CSS stylesheet for Color Picker
             wp_enqueue_style('wp-color-picker');
             wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/wp-biscotti-admin.css', array( 'wp-color-picker' ), $this->version, 'all');
        }
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wp_Biscotti_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wp_Biscotti_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if ('settings_page_wp-biscotti' == get_current_screen() -> id) {
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/wp-biscotti-admin.js', array( 'jquery', 'wp-color-picker' ), $this->version, false);
        }
    }
    
    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu()
    {
        add_options_page('WP GDPR Banner', 'WP GDPR', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page'));
    }
    
    /**
    * Add settings action link to the plugins page.
    *
    * @since    1.0.0
    */

    public function add_action_links($links)
    {
        $settings_link = array(
        '<a href="' . admin_url('options-general.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
        );
        return array_merge($settings_link, $links);
    }
    
    /**
     * Render the settings page for this plugin.
     *
     * @since    1.0.0
     */

    public function display_plugin_setup_page()
    {
        include_once('partials/wp-biscotti-admin-display.php');
    }
    
    /**
     * Validates settings for this plugin.
     *
     * @since    1.0.0
     */
    public function validate($input)
    {
        // All  inputs
        $valid = array();

        // Set default colors if settings are empty
        $valid['modal_background_color'] = ( isset($input['modal_background_color']) && !empty($input['modal_background_color']) ) ? $input['modal_background_color'] : '#ffffff';
        $valid['modal_text_color'] = empty($input['modal_text_color']) ? '#808080' : $input['modal_text_color'];
        $valid['modal_primary_color'] = empty($input['modal_primary_color']) ? '#309DB5' : $input['modal_primary_color'];

        // Copy text as is
        $valid['banner_content_standard'] = $input['banner_content_standard'];
        $valid['banner_content_why'] = $input['banner_content_why'];
        $valid['banner_content_confirm'] = $input['banner_content_confirm'];
        $valid['banner_content_limited'] = $input['banner_content_limited'];
        $valid['head_code_using_cookies'] = $input['head_code_using_cookies'];
        $valid['body_code_using_cookies'] = $input['body_code_using_cookies'];
        
        // Font sizes
        $valid['modal_title-size'] = ( isset($input['modal_title-size']) && !empty($input['modal_title-size']) ) ? $input['modal_title-size'] : '1.4rem';
        $valid['modal_content-size'] = ( isset($input['modal_content-size']) && !empty($input['modal_content-size']) ) ? $input['modal_content-size'] : '.8rem';
        $valid['modal_button-size'] = ( isset($input['modal_button-size']) && !empty($input['modal_button-size']) ) ? $input['modal_button-size'] : '1.1rem';
        $valid['modal_subbtn-size'] = ( isset($input['modal_subbtn-size']) && !empty($input['modal_subbtn-size']) ) ? $input['modal_subbtn-size'] : '.75rem';
        
        // Modal is active
        $valid['modal_active'] =  ( isset($input['modal_active']) && !empty($input['modal_active']) ) ? 1 : 0;
        
        return $valid;
    }
    
    /**
     * Saves/updates settings for this plugin.
     *
     * @since    1.0.0
     */
    public function options_update()
    {
        register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
    }
    
    /**
     *  Add a custom api endpoint to manage cookies via ajax.
     *
     * @since    1.0.0
     */
    public function add_api_endpoint()
    {
        register_rest_route('wp-biscotti/v1', '/setcookie', array(
            'methods' => 'POST',
            'callback' => array($this, 'wpb_setcookie'),
        ));
    }
    
    /**
     *  Setcookie callback for custom api endpoint.
     *
     * @since    1.0.0
     */
    public function wpb_setcookie(WP_REST_Request $request)
    {
        $value = $request->get_param('cookieValue');
        setcookie("wpb_gdpr_cookie", $value, time()+2592000, '/');
    }
}
