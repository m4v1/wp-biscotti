<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.olos.com
 * @since      1.0.0
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/includes
 * @author     Marco Vivi <marco.vivi@gmail.com>
 */
class Wp_Biscotti_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
