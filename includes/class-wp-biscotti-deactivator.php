<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.olos.com
 * @since      1.0.0
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/includes
 * @author     Marco Vivi <marco.vivi@gmail.com>
 */
class Wp_Biscotti_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
