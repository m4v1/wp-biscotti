<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.olos.com
 * @since      1.0.0
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/public
 * @author     Marco Vivi <marco.vivi@gmail.com>
 */
class Wp_Biscotti_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    
    /**
     * If the plugin is active.
     *
     * @since    1.2.2
     * @access   private
     * @var      bool    $isActive    If the plugin is active.
     */
    private $isActive;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $options = get_option($this->plugin_name);
        if (isset($options['modal_active']) && !empty($options['modal_active']) && $options['modal_active'] == 1) {
            $this->isActive = true;
        }
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wp_Biscotti_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wp_Biscotti_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        if ($this->isActive) {
            wp_enqueue_style('jquery-modal-style', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css', array(), '0.9.1', 'all');
            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/wp-biscotti-public.css', array(), $this->version, 'all');
        }
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wp_Biscotti_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wp_Biscotti_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        if ($this->isActive) {
            wp_enqueue_script('js-cookie', 'https://cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js', array(), '2.2.0', false);
            wp_enqueue_script('jquery-modal-script', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', array( 'jquery' ), '0.9.1', true);
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/wp-biscotti-public.js', array( 'jquery' ), $this->version, true);
        }
    }
    
    /**
     * Convert hexdec color string to rgb(a) string.
     *
     * @since    1.0.0
     */
    function hex2rgb($colour)
    {
        if ($colour[0] == '#') {
                $colour = substr($colour, 1);
        }
        if (strlen($colour) == 6) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
        } elseif (strlen($colour) == 3) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
        } else {
                return false;
        }
        $r = hexdec($r);
        $g = hexdec($g);
        $b = hexdec($b);
        
        return $r . ', ' . $g . ', ' . $b;
    }
    
    /**
     * Displays the modal with the banner.
     *
     * @since    1.0.0
     */
    public function display_banner()
    {
        if ($this->isActive) {
            include_once('partials/wp-biscotti-public-display.php');
        }
    }
    
    /**
     * Check if Cookies are allowed.
     *
     * @since    1.0.0
     */
    public function cookies_allowed()
    {
        if (isset($_COOKIE['wpb_gdpr_cookie']) && !empty($_COOKIE['wpb_gdpr_cookie']) && !empty($_COOKIE['wpb_gdpr_cookie'])) {
            if ($_COOKIE['wpb_gdpr_cookie'] == 'allow') {
                return true;
            } else {
                return false;
            }
        }
    }
    
    /**
     * Display the code that uses cookies.
     *
     * @since    1.0.0
     */
    public function display_code_using_cookies()
    {
        if ($this->cookies_allowed() && $this->isActive) {
            add_action('wp_head', array($this, 'append_head_code'));
            add_action('wp_footer', array($this, 'append_body_code'));
        }
    }
    
    /**
     * Append HEAD code in wp_head.
     *
     * @since    1.0.0
     */
    public function append_head_code()
    {
        $options = get_option($this->plugin_name);
        if (isset($options['head_code_using_cookies']) && !empty($options['head_code_using_cookies'])) {
            echo $options['head_code_using_cookies'];
        }
    }
    
    /**
     * Append BODY code in wp_footer.
     *
     * @since    1.0.0
     */
    public function append_body_code()
    {
        $options = get_option($this->plugin_name);
        if (isset($options['body_code_using_cookies']) && !empty($options['body_code_using_cookies'])) {
            echo $options['body_code_using_cookies'];
        }
    }
}
