<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.olos.com
 * @since      1.0.0
 *
 * @package    Wp_Biscotti
 * @subpackage Wp_Biscotti/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
//Grab all options
$options = get_option($this->plugin_name);

// Modal customization vars
$modal_background_color = $options['modal_background_color'];
$modal_text_color = $options['modal_text_color'];
$modal_primary_color = $options['modal_primary_color'];

// Modal font sizes
$wpb_title_size = $options['modal_title-size'];
$wpb_content_size = $options['modal_content-size'];
$wpb_button_size = $options['modal_button-size'];
$wpb_subbtn_size = $options['modal_subbtn-size'];

// Format button's background to rgb
$button_bg = $this->hex2rgb($modal_primary_color);

// Text contents
$banner_content_standard = apply_filters('the_content', $options['banner_content_standard']);
$banner_content_why = apply_filters('the_content', $options['banner_content_why']);
$banner_content_confirm = apply_filters('the_content', $options['banner_content_confirm']);
$banner_content_limited = apply_filters('the_content', $options['banner_content_limited']);

?>

<style media="screen">
    .wpb-banner {
         background-color: <?php echo $modal_background_color; ?> !important;
         color: <?php echo $modal_text_color; ?> !important;
    }
    .wpb-banner h3 {
         color: <?php echo $modal_text_color; ?> !important;
         font-size: <?php echo $wpb_title_size; ?> !important;
         
    }
    #wpb-banner-why h3, #wpb-banner-limited h3 {
        background-color: <?php echo $modal_primary_color; ?> !important;
        color: <?php echo $modal_background_color; ?> !important;
    }
    .wpb-banner .wpb-content {
        font-size: <?php echo $wpb_content_size; ?> !important;
    }
    .wpb-banner .wpb-buttons button {
        background: <?php echo $modal_primary_color; ?> !important;
        --bg-color: <?php echo $button_bg; ?> !important;
        color: <?php echo $modal_background_color; ?> !important;
        font-size: <?php echo $wpb_button_size; ?> !important;
    }
    .wpb-banner .wpb-buttons .wpb-more {
        color: <?php echo $modal_primary_color; ?> !important;
        font-size: <?php echo $wpb_subbtn_size; ?> !important;
    }
</style>

<div id="wpb-banner-standard" class="modal wpb-banner">
  <h3 class="wpb-title">La tua privacy mi interessa</h3>
  <div class="wpb-content"><?php echo $banner_content_standard; ?></div>
  <div class="wpb-buttons">
      <button type="button" class="wpb-button-banner-standard" name="button-standard">Accetto</button>
      <span class="wpb-more" id="wpb-more">Dimmi di più</span>
  </div>
</div>

<div id="wpb-banner-why" class="modal wpb-banner">
  <h3 class="wpb-title">Perché mi serve il tuo consenso</h3>
  <div class="wpb-content"><?php echo $banner_content_why; ?></div>
  <div class="wpb-buttons">
      <button type="button" class="wpb-button-banner-why" name="button-why">Accetto e continuo</button>
      <span class="wpb-more" id="wpb-deny">Non voglio accettare</span>
  </div>
</div>

<div id="wpb-banner-confirm" class="modal wpb-banner">
  <h3 class="wpb-title">Sei sicuro?</h3>
  <div class="wpb-content"><?php echo $banner_content_confirm; ?></div>
  <div class="wpb-buttons">
      <button type="button" class="wpb-button-banner-confirm" name="button-confirm">Torna indietro</button>
      <span class="wpb-more"  id="wpb-limited">Navigazione limitata</span>
  </div>
</div>

<div id="wpb-banner-limited" class="modal wpb-banner">
  <h3 class="wpb-title">Navigazione limitata</h3>
  <div class="wpb-content"><?php echo $banner_content_limited; ?></div>
  <div class="wpb-buttons">
      <button type="button" class="wpb-button-banner-limited" name="button-limited">Accetto e continuo</button>
      <span class="wpb-more"  id="wpb-back">Torna indietro per vedere i dettagli</span>
  </div>
</div>

<script type="text/javascript">

var wpbCookie = Cookies.get("wpb_gdpr_cookie");
var cookieIsSet = false;

if (typeof wpbCookie !== 'undefined') {
    cookieIsSet = true; 
}

jQuery(document).ready(function($){
    
    if (cookieIsSet == false) {
        
        const modalSettings = {
            clickClose: false,
            escapeClose: false,
            showClose: false,
        };
        
        $("#wpb-banner-standard").modal(modalSettings);
        
    } else if (wpbCookie == 'disallow') {
        
        const modalSettings = {
            clickClose: false,
            escapeClose: false,
            showClose: false,
            blockerClass: "jquery-modal wpb-transparent"
        };
        
        $("#wpb-banner-standard").modal(modalSettings);
        
    }
    
}); 

</script>
