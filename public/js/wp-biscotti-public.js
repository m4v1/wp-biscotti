(function( $ ) {
	'use strict';

	function sendCookie(value) {
        $.ajax({
          type: "POST",
          url: "/wp-json/wp-biscotti/v1/setcookie",
          dataType: 'json',
          data: {
              cookieValue: value,
          },
          success: function() {
              location.reload();
          },
          error: function(e) {
			  console.log(e);
              alert('Error occured');
          }
      });
    };
    
    if (cookieIsSet == false || wpbCookie == 'disallow') {
		
		// Configure modal settings
		const modalSettings = {
            clickClose: false,
            escapeClose: false,
            showClose: false,
        };
		
		// Onclick events for main buttons
	    $( "#wpb-banner-standard button" ).on( "click", function() {
	      $.modal.close();
	      sendCookie('allow');
	    });
	    $( "#wpb-banner-why button" ).on( "click", function() {
	      $.modal.close();
	      sendCookie('allow');
	    });
	    $( "#wpb-banner-confirm button" ).on( "click", function() {
	      $("#wpb-banner-why").modal(modalSettings);
	    });
	    $( "#wpb-banner-limited button" ).on( "click", function() {
	      $.modal.close();
	      sendCookie('disallow');
	    });
	    
	    // Onclick events for sub buttons
	    $( "#wpb-banner-standard #wpb-more" ).on( "click", function() {
	      $("#wpb-banner-why").modal(modalSettings);
	    });
	    $( "#wpb-banner-why #wpb-deny" ).on( "click", function() {
	      $("#wpb-banner-confirm").modal(modalSettings);
	    });
	    $( "#wpb-banner-confirm #wpb-limited" ).on( "click", function() {
	      $("#wpb-banner-limited").modal(modalSettings);
	    });
	    $( "#wpb-banner-limited #wpb-back" ).on( "click", function() {
	      $("#wpb-banner-why").modal(modalSettings);
	    });
    }

})( jQuery );
