# WP Biscotti GDPR!

WP Biscotti GDPR è un semplice plugin per **Wordpress** che cerca di offrire una soluzione completa per la gestione del banner necessario per ovviare ai requisiti del **GDPR**.
Il plugin consentirà di accedere al sito in 2 modalità:

- Navigazione anonima senza cookies
	> In questa modalità non viene tracciato l'utente e non vengono caricati sistemi di analitica.
- Navigazione normale con i cookies
	> In questa modalità l'utente, che ha acconsentito al trattamento, viene tracciato attraverso i sistemi di analitica presenti nel sito web.
